#!/usr/bin/env bash

display_error()
# Displaying for error
# Arguments:
# MSG (string): error message
{
echo -e "\n\e[1;31m${MSG}\e[0m\n"
}

declare -gA mule_apiId

function deploy_api_mgr() {
    echo "check api_mgr"
    #https://anypoint.mulesoft.com/exchange/api/v1/assets/b885d972-2a66-42b0-9a46-78aebb2fe5b9/iqvia-rds-cdr-studydata-sys/2.0.1
    #local apiMgrURL="https://anypoint.mulesoft.com/apimanager/api/v1/organizations/$6/environments/$5/apis?assetId=$2&assetVersion=$3"
    local apiMgrURL="https://anypoint.mulesoft.com/exchange/api/v1/assets/$6/$2/$3"
    #local apiId=$(curl -s -H 'Authorization: Bearer '$4 -X GET "$apiMgrURL"| jq '.assets[].apis[] | select(."lastActiveDate" != null) | ."id"' -r)
    local apiId=$(curl -s -H 'Authorization: Bearer '$4 -X GET "$apiMgrURL"| jq '.instances | map(select(."environmentId" == "'$5'").id) | first' -r)
    if [ ! "$apiId" != "null" ] && [ "$5" == "d1ca01ab-bf18-43f1-8347-023fb36dab93" ]; then
        echo "deploy_api_mgr"
        local manageURL="https://anypoint.mulesoft.com/apimanager/api/v1/organizations/$6/environments/$5/apis"
    	local manageURLData='{"endpoint":{"deploymentType":"CH","isCloudHub":true,"muleVersion4OrAbove":'$1',"proxyUri":null,"referencesUserDomain":null,"responseTimeout":null,"type":"rest-api","uri":null},"instanceLabel":null,"spec":{"assetId":"'$2'","groupId":"'$6'","version":"'$3'"}}'
    	local setAPIMgrcmd=$(curl -s -H 'Content-Type: application/json' -H 'Authorization: Bearer '$4 -H 'x-anypnt-env-id: '$5 -H 'x-anypnt-org-id: '$6 -d "$manageURLData" -X POST "$manageURL")
        echo "Line130 local response: ${setAPIMgrcmd}"
        apiCreate="true"
    fi
    
    promoteFrom=null
    if [ "$9" != "null" ]; then
        promoteFromProfile=$(jq -r ".\"$9\".\"anypointCliProfile\"" environments.json)
        export promoteFromProfile
        promoteFrom=$(jq ".\"$promoteFromProfile\".\"environmentId\""  ~/.anypoint/credentials -r)
    fi
    
    if [ "$apiId" == "null" ] && [ "$promoteFrom" != "null" ]; then
        echo "promote api"
        apiId=$(curl -s -H 'Authorization: Bearer '$4 -X GET "$apiMgrURL"| jq '.instances | map(select(."environmentId" == "'$promoteFrom'").id) | first' -r)
        local promoteURL="https://anypoint.mulesoft.com/apimanager/api/v1/organizations/$6/environments/$5/apis"
        local promoteData='{ "promote": { "originApiId": '$apiId', "policies": { "allEntities": true }, "tiers": { "allEntities": true }, "alerts": {"allEntities":true } } }'
    	local promoteApi=$(curl -s -H 'Content-Type: application/json' -H 'Authorization: Bearer '$4 -d "$promoteData" -X POST "$promoteURL")
        echo "Line130 local response: ${promoteApi}"
        apiCreate="true"
    fi
    #apiMgrURL="https://anypoint.mulesoft.com/apimanager/api/v1/organizations/$6/environments/$5/apis?assetId=$2&assetVersion=$3"
    #apiId=$(curl -s -H 'Authorization: Bearer '$4 -X GET "$apiMgrURL"| jq '.assets[].apis[] | select(."lastActiveDate" != null) | ."id"' -r)
    apiMgrURL="https://anypoint.mulesoft.com/exchange/api/v1/assets/$6/$2/$3"
    apiId=$(curl -s -H 'Authorization: Bearer '$4 -X GET "$apiMgrURL"| jq '.instances | map(select(."environmentId" == "'$5'").id) | first' -r)
    
    mule_apiId=$apiId
    
    if [ "$7" == "true" ] && [ "$apiId" != "null" ] ; then
        getAppId=$(curl -s -H 'Content-Type: application/json' -H "Authorization: Bearer $APIToken" -X GET "https://anypoint.mulesoft.com/apiplatform/repository/v2/organizations/$OrgID/apis/versions/contracts/applications" | jq -r ".[] | select(.\"name\" == \"$8\") | .\"id\"")
        if [ ! -n "${getAppId}" ]; then
          getAppId=$(curl -s -H 'Content-Type: application/json' -H "Authorization: Bearer $APIToken" -d "{\"name\":\"$8\"}" -X POST "https://anypoint.mulesoft.com/apiplatform/repository/v2/organizations/$OrgID/applications" | jq -r ".id")
        fi
        
        local credentialsResp=$(curl -s -o response.txt -w "%{http_code}" -H "Authorization: Bearer $APIToken" -X GET "https://anypoint.mulesoft.com/apiplatform/repository/v2/organizations/$6/applications/$getAppId")
        if [ "$credentialsResp" == "200" ]; then
	        local response2=$(curl -s -H 'Content-Type: application/json' -H "Authorization: Bearer $APIToken" -d "{\"apiVersionId\":$apiId, \"applicationId\":$getAppId, \"partyId\":\"\", \"partyName\":\"\", \"acceptedTerms\":false}" -X POST "https://anypoint.mulesoft.com/apimanager/api/v1/organizations/$6/environments/$5/apis/$apiId/contracts")
			
            credentialsResp=$(curl -s -H "Authorization: Bearer $APIToken" -X GET "https://anypoint.mulesoft.com/apiplatform/repository/v2/organizations/$6/applications/$getAppId")
			
			local clientId=$(echo $credentialsResp | jq -r '."clientId"')
			local clientSecret=$(echo $credentialsResp | jq -r '."clientSecret"')
	    fi
    fi
    
    #echo -n > $assetId
     #for Mule4 or Above - use ApiID
    #if [ "$1" == "true" ]; then
     #   echo $apiId >> $assetId
    #else 
     #   echo $autodiscoveryname >> $assetId
    #fi
}

declare -A dynamic_properties
 
function init_dynamic_properties() {
    PREFIXPRODUCT="rds"
    echo "init_dynamic_properties"
    #echo "PREFIXPRODUCT : ${PREFIXPRODUCT}"
 
    #local urlVersion=$(jq -r ".\"$bundle_environment\".\"urlVersion\"" bundle.json)
    local urlRegion=$(jq -r ".\"$bundle_environment\".\"urlRegion\"" environments.json)
    local urlEnvironment=$(jq -r ".\"$bundle_environment\".\"urlEnvironment\"" environments.json)
	local cloudHubDomain=$(jq -r ".\"$bundle_environment\".\"cloudHubDomain\"" environments.json)
	
 
    #automationUsername#
    #dynamic_properties["automationUsername"]=$Username
 
    #automationPassword#
    #dynamic_properties["automationPassword"]=$Password
 
    #environmentId#
    #dynamic_properties["environmentId"]=$EnvironmentID
 
    #organizationId#
    #dynamic_properties["organizationId"]=$OrgID
     
    #parentOrganizationId#
    #dynamic_properties["parentOrganizationId"]=$MasterOrgID
 
    #loggerApiURL#
    local loggerApiPropertyFile=$(jq -r ".\"$bundle_environment\".\"loggerApiPropertyFile\"" environments.json)
    local loggerApiUrlString=$(jq -r ".\"$loggerApiPropertyFile\".\"urlString\"" bundle.json)
    #dynamic_properties["loggerApiURL"]="https://${PREFIXPRODUCT}-"$loggerApiUrlString"-"$urlVersion"-"$urlEnvironment"."$cloudHubDomain"/api"
    local loggerApiURL=$(jq -r ".\"$bundle_environment\".\"loggerApiURL\"" environments.json)
    dynamic_properties["loggerApiURL"]="https://"${loggerApiURL}"."$cloudHubDomain"/api"
    dynamic_properties["loggerClientId"]=$(jq -r ".\"$bundle_environment\".\"loggerClientId\"" environments.json)
    dynamic_properties["loggerClientSecret"]=$(jq -r ".\"$bundle_environment\".\"loggerClientSecret\"" environments.json)
    
    #platformApisPath#
    #dynamic_properties["platformApisPath"]="/apimanager/api/v1/organizations/$OrgID/environments/$EnvironmentID/apis"
     
    #platformApplicationsPath#
    #dynamic_properties["platformApplicationsPath"]="/apiplatform/repository/v2/organizations/$OrgID/applications"
     
    #configApiHost#
    local configApiPropertyFile=$(jq -r ".\"$bundle_environment\".\"configApiPropertyFile\"" environments.json)
    local configApiUrlString=$(jq -r ".\"$configApiPropertyFile\".\"urlString\"" bundle.json)
    dynamic_properties["configApiHost"]="${COMPANY}-${PREFIXPRODUCT}-"$configApiUrlString"-"$urlVersion"-"$urlEnvironment"."$cloudHubDomain
    
    #fileApiHost#
    #local fileApiPropertyFile=$(jq -r ".\"$bundle_environment\".\"fileApiPropertyFile\"" environments.json)
    #local fileApiUrlString=$(jq -r ".\"$fileApiPropertyFile\".\"urlString\"" bundle.json)
    #dynamic_properties["fileApiHost"]="${COMPANY}-${PREFIXPRODUCT}-"$fileApiUrlString"-"$urlVersion"-"$urlEnvironment"-"$urlRegion"."$cloudHubDomain
     
     
    if [ ! -z "$InternalConsumerAppId" ]; then
      local response=$(curl -s -H 'Content-Type: application/json' -H "Authorization: Bearer $APIToken" -X GET "https://anypoint.mulesoft.com/cloudhub/api/v2/applications/$InternalConsumerAppId")
      echo "Line130 local response: ${response}"
     
      #internalClientId#
      dynamic_properties["internalClientId"]=$(echo $response | jq '."clientId"')
     
      #internalClientSecret#
      dynamic_properties["internalClientSecret"]=$(echo $response | jq '."clientSecret"')
    fi
}
 
declare -A properties_list
 
function gen_properties() {
    echo "gen_properties..."
    apiVersion=$(cat $2)
    echo $apiVersion
    local properties=$(jq ".\"$1\".\"properties\"" bundle.json | jq -r 'keys[]')
    for p in $(echo $properties)   
    do
        if [ "$3" == "false" ] && [ "$p" == "apiVersion" ]; then
            continue
        fi
        local lookup=$(jq -r ".\"$1\".\"properties\".\"$p\"" bundle.json)
        local value=$(jq -r ".\"$bundle_environment\".\"$lookup\"" environments.json)
        if [[ $lookup == "#apiName#" ]]; then
            value="groupId:"$OrgID":assetId:"$2
        elif [[ $lookup == "#apiVersion#" ]]; then
            #value=${api_version["$2"]}
            value=$apiVersion
        elif [[ $lookup == "#"* ]]; then
            local dlookup=$(echo $lookup | sed 's/[#]//g')
            value=${dynamic_properties["$dlookup"]} 
        fi
        #echo $p"="$value
		#properties_list[$1]+=" --property \"$p\\=$value\""
		properties_list[$1]+="\"$p\":\"$value\","
    done
}

function gen_properties_mule4() {
    echo "gen_properties_mule4..."
    echo "2222 $2"
    apiId=$2
    #echo $apiId
    local properties=$(jq ".\"$1\".\"properties\"" bundle.json | jq -r 'keys[]')
    for p in $(echo $properties)   
    do
        if [ "$3" == "false" ] && [ "$p" == "api.id" ]; then
            continue
        fi
        local lookup=$(jq -r ".\"$1\".\"properties\".\"$p\"" bundle.json)
        local value=$(jq -r ".\"$bundle_environment\".\"$lookup\"" environments.json)
        
        if [[ $lookup == "#apiId#" ]]; then
            value=$mule_apiId
            #value=15898518
        elif [[ $lookup == "#"* ]]; then
            local dlookup=$(echo $lookup | sed 's/[#]//g')
            value=${dynamic_properties["$dlookup"]} 
        fi
        #echo $p"="$value
		properties_list[$1]+="\"$p\":\"$value\","
    done
}

function deploy_runtime() {
    echo "deploying" $1
    if [[ $1 == *.jar ]]; then
        local devpropertyfile=$(unzip -l $1 config/*.$bundle_environment.properties | grep properties | sed 's/config\///g' | awk '{print $NF}')
        local assetid=$(unzip -p $1 config/$devpropertyfile | grep apiName= | awk 'BEGIN {FS=":"} {printf "%s",$4}' | sed 's/[\r]//g')
        if [[ -z ${assetid} ]]; then MSG="The assetid variable is empty, so there is an issue with the configuration for apiId in the ${devpropertyfile} file !!"; display_error ${MSG}; fi
    else
        local devpropertyfile=$(unzip -l $1 classes/*.$bundle_environment.properties | grep properties | sed 's/classes\///g' | awk '{print $NF}')
        local assetid=$(unzip -p $1 classes/$devpropertyfile | grep apiName= | awk 'BEGIN {FS=":"} {printf "%s",$4}' | sed 's/[\r]//g')
        if [[ -z ${assetid} ]]; then MSG="The assetid is empty, so there is an issue with the configuration for apiName and apiVersion in the ${devpropertyfile} file !!"; display_error ${MSG}; fi
    fi

    apiMuleVersion4OrAbove=$(jq -r ".\"$devpropertyfile\".\"muleVersion4OrAbove\"" bundle.json)

    local urlVersion=$(jq -r ".\"$devpropertyfile\".\"urlVersion\"" bundle.json)
    #echo "urlVersion $urlVersion"
    local urlRegion=$(jq -r ".\"$bundle_environment\".\"urlRegion\"" environments.json)
    local urlEnvironment=$(jq -r ".\"$bundle_environment\".\"urlEnvironment\"" environments.json)
    local zipOnly=$(jq -r ".\"$bundle_environment\".\"zipOnly\"" environments.json)
    local jarOnly=$(jq -r ".\"$bundle_environment\".\"jarOnly\"" environments.json)
	local cloudHubRegion=$(jq -r ".\"$bundle_environment\".\"cloudHubRegion\"" environments.json)
	
    local workers=$(jq -r ".\"$devpropertyfile\".\"workers\"" bundle.json)
    local workerSize=$(jq -r ".\"$devpropertyfile\".\"workerSize\"" bundle.json)
    local runtime=$(jq -r ".\"$devpropertyfile\".\"runtime\"" bundle.json)
    local runtimeMule4=$(jq -r ".\"$devpropertyfile\".\"runtimeMule4\"" bundle.json)
    local objectStoreV2=$(jq -r ".\"$devpropertyfile\".\"objectStoreV2\"" bundle.json)
	local oracleURL=$(jq -r ".\"$devpropertyfile\".\"properties\".\"db.oracle.url\"" bundle.json)
	local action="deploy"
     
    if [ "$workers" == "null" ]; then
        workers=$(jq -r ".\"$bundle_environment\".\"workers\"" environments.json)
    fi
     
    if [ "$workerSize" == "null" ]; then
        workerSize=$(jq -r ".\"$bundle_environment\".\"workerSize\"" environments.json)
    fi
     
    if [ "$runtime" == "null" ]; then
        runtime=$(jq -r ".\"$bundle_environment\".\"runtime\"" environments.json)
    fi
    if [ "$runtimeMule4" == "null" ]; then
        runtimeMule4=$(jq -r ".\"$bundle_environment\".\"runtimeMule4\"" environments.json)
    fi 
    local grantAccessInternalApp=$(jq -r ".\"$devpropertyfile\".\"grantAccessInternalApp\"" bundle.json)

    local ApiUrlString=$(jq -r ".\"$devpropertyfile\".\"urlString\"" bundle.json)
    
    local version=$(jq -r ".\"$devpropertyfile\".\"version\"" bundle.json)
    
    assetid=$(jq -r ".\"$devpropertyfile\".\"assetId\"" bundle.json)
    if [ ! -n "$assetid" ] || [ "$assetid" == "null" ]; then
        assetid="${COMPANY}-${PREFIXPRODUCT}-$ApiUrlString"
        echo "Get Asset Id ${COMPANY}-${PREFIXPRODUCT}-$ApiUrlString"
    fi
    echo $assetid
    
    apiName=$(jq -r ".\"$devpropertyfile\".\"apiName\"" bundle.json)
    if [ ! -n "$apiName" ] || [ "$apiName" == "null" ]; then
        apiName="${COMPANY}-${PREFIXPRODUCT}-$ApiUrlString-$urlVersion-$urlEnvironment"
        if [ "$urlEnvironment" == "prod" ]; then
            apiName="${COMPANY}-${PREFIXPRODUCT}-$ApiUrlString-$urlVersion"
        fi
    fi
    echo $apiName
    
    promoteFromEnv=$(jq -r ".\"$devpropertyfile\".\"promoteFrom\"" bundle.json)
    if [ ! -n "$promoteFromEnv" ] || [ "$promoteFromEnv" == "null" ]; then
        echo "promoteApi is not applicable hence skipped"
        promoteFromEnv=null
    fi
    
    apiCreate="false"
    deploy_api_mgr "$apiMuleVersion4OrAbove" "$assetid" "$version" "$APIToken" "$EnvironmentID" "$OrgID" "$grantAccessInternalApp" "$apiName" "$promoteFromEnv"
    #echo "api manager apiId === $assetId"
    
    if [ "$apiMuleVersion4OrAbove" == "true" ]; then
        gen_properties_mule4 $devpropertyfile $assetId $apiCreate
        runtimeToUse=$runtimeMule4
    else
        gen_properties $devpropertyfile $assetId $apiCreate
        runtimeToUse=$runtime 
    fi

    allProps=${properties_list[$devpropertyfile]}
    allProps=${allProps%?}
    allProps="{"$allProps"}"

    muleRegion=$(echo $allProps | jq '."mule.region"')
    
    #uploadformData='{"domain": "'"$apiName"'", "muleVersion": {"version": "'"$runtimeToUse"'"}, "region": '$muleRegion', "workers": {"amount": '$workers', "type": {"name": "Micro"}}, "monitoringEnabled": true, "monitoringAutoRestart": true, "objectStoreV1": false, "properties": '$allProps'}'
    #eval $uploadformData
    #echo $uploadformData    

	#local deploy_cmd="anypoint-cli runtime-mgr cloudhub-application 
	#$action --runtime=$runtimeToUse 
	#--region=$cloudHubRegion $workersOption $workerSizeOption 
	#$propertiesOption ${COMPANY}-${PREFIXPRODUCT}-$ApiUrlString-$urlVersion-$urlEnvironment-$urlRegion $1"
	#echo $deploy_cmd
    #eval $deploy_cmd
    
    filePath=$PWD"/"$1
	
	appExists="true"
	local response=$(curl -s -o response.txt -w "%{http_code}" -H 'Content-Type: application/json' -H "Authorization: Bearer $APIToken" -H "x-anypnt-env-id: $EnvironmentID" -H "x-anypnt-org-id: $OrgID" -X GET "https://anypoint.mulesoft.com/cloudhub/api/v2/applications/$apiName")
	#echo $response
	if [ "$response" == "404" ]; then
	    appExists="false"
	fi
	
	
	if [ "$appExists" == "true" ]; then
        #action="modify"
        workersOption=""
        workerSizeOption=""
        if [ "$apiMuleVersion4OrAbove" == "true" ]; then
            if [ "$jarOnly" == "true" ]; then
                propertiesOption=""
				# curl for only jar
            else
                python ./deploy.py $EnvironmentID $APIToken '{"muleVersion": {"version": "'"$runtimeToUse"'"}, "region": '$muleRegion', "workers": {"amount": '$workers', "type": {"name": "Micro", "weight": '$workerSize'}}, "monitoringEnabled": true, "monitoringAutoRestart": true, "objectStoreV1": "'"$objectStoreV2"'", "properties": '$allProps'}' $filePath $appExists $apiName
            fi
        else
             if [ "$zipOnly" == "true" ]; then
                propertiesOption=""
				# curl for only zip
            else
                #propertiesOption="${properties_list[$devpropertyfile]}"
				python ./deploy.py $EnvironmentID $APIToken '{"muleVersion": {"version": "'"$runtimeToUse"'"}, "region": '$muleRegion', "workers": {"amount": '$workers', "type": {"name": "Micro", "weight": '$workerSize'}}, "monitoringEnabled": true, "monitoringAutoRestart": true, "objectStoreV1": "'"$objectStoreV2"'", "properties": '$allProps'}' $filePath $appExists $apiName
            fi
        fi
    else
        #workersOption="--workers=$workers"
        #workerSizeOption="--workerSize=$workerSize"
        #propertiesOption="${properties_list[$devpropertyfile]}"
        python ./deploy.py $EnvironmentID $APIToken '{"domain": "'"$apiName"'", "muleVersion": {"version": "'"$runtimeToUse"'"}, "region": '$muleRegion', "workers": {"amount": '$workers', "type": {"name": "Micro", "weight": '$workerSize'}}, "monitoringEnabled": true, "monitoringAutoRestart": true, "objectStoreV1": "'"$objectStoreV2"'", "properties": '$allProps'}' $filePath $appExists $apiName
    fi

	#echo "App Exist?$appExists" 
	#eval $deploy_cmd
    
	if [ "$oracleURL" != "null" ]; then
		echo "setting Oracle URL"
        local response=$(curl -s -H 'Content-Type: application/json' -H "Authorization: Bearer $APIToken" -H "x-anypnt-env-id: $EnvironmentID" -H "x-anypnt-org-id: $OrgID" -X GET "https://anypoint.mulesoft.com/cloudhub/api/v2/applications/${COMPANY}-${PREFIXPRODUCT}-$ApiUrlString-$urlVersion-$urlEnvironment-$urlRegion")
    	local oracleURLValue=$(jq -r ".\"$bundle_environment\".\"$oracleURL\"" environments.json)
		echo $oracleURLValue
		response=$(echo $response | jq ".properties.\"db.oracle.url\"=\"$oracleURLValue\"" | jq ".properties")
		echo $response
		local setOracleURLcmd="curl -H \"Content-Type: application/json\" -H \"authorization: Bearer $APIToken\" -H \"x-anypnt-env-id: $EnvironmentID\" -H \"x-anypnt-org-id: $OrgID\" -d '{\"properties\":$response}' -X PUT https://anypoint.mulesoft.com/cloudhub/api/v2/applications/${COMPANY}-${PREFIXPRODUCT}-$ApiUrlString-$urlVersion-$urlEnvironment-$urlRegion"
        #echo $setOracleURLcmd
		eval $setOracleURLcmd
        #anypoint-cli runtime-mgr cloudhub-application restart "${COMPANY}-${PREFIXPRODUCT}-$ApiUrlString-$urlVersion-$urlEnvironment-$urlRegion"
	fi
}
 
usage() {
    echo "Lexi deployment 0.1 (beta)"
    echo "Usage: $0 -e <environment> [-z <zip file>]"
    echo "*** all zip files under runtime-mgr will be deployed if -z is omitted ***"
    exit 1;
}
 
while getopts ":e:z:" o; do
    case "${o}" in
        e)
            bundle_environment=${OPTARG}
             
            ;;
        z)
            zip_or_jar_file=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))
 
if [ -z "${bundle_environment}" ]; then
    usage
fi
 
#echo $bundle_environment
ANYPOINT_PROFILE=$(jq -r ".\"$bundle_environment\".\"anypointCliProfile\"" environments.json)
#echo $ANYPOINT_PROFILE
#Username=$(jq ".\"$ANYPOINT_PROFILE\".\"username\""  ~/.anypoint/credentials -r)
 
#if [ "${ANYPOINT_PROFILE}" == "null" ] || [ -z "${ANYPOINT_PROFILE}" ]; then
 #   echo "Invalid environment or environment.json"
  #  usage
#fi
 
#if [ "${Username}" == "null" ] || [ -z "${Username}" ]; then
 #   echo "Invalid Anypoint CLI profile"
  #  usage
#fi
 
if [ ! -z "$zip_or_jar_file" ] && [ ! -f runtime-mgr/"$zip_or_jar_file" ]; then
    echo "Zip file not found"
    usage
fi

export ANYPOINT_PROFILE
 
Username=$(jq ".\"$ANYPOINT_PROFILE\".\"username\""  ~/.anypoint/credentials -r)
Password=$(jq ".\"$ANYPOINT_PROFILE\".\"password\""  ~/.anypoint/credentials -r)
EnvironmentID=$(jq ".\"$ANYPOINT_PROFILE\".\"environmentId\""  ~/.anypoint/credentials -r)
OrgID=$(jq ".\"$ANYPOINT_PROFILE\".\"orgId\""  ~/.anypoint/credentials -r)
MasterOrgID=$(jq ".\"$ANYPOINT_PROFILE\".\"masterOrgId\""  ~/.anypoint/credentials -r)

InternalConsumerApp=$(jq -r ".\"$bundle_environment\".\"internalConsumerApplication\"" environments.json)
 
APIToken=$(curl -s -H "Content-Type: application/json" -X POST -d '{"username":"'"$Username"'","password":"'"$Password"'"}' https://anypoint.mulesoft.com/accounts/login | jq '.access_token' -r)
 
InternalConsumerAppId=$(curl -s -H 'Content-Type: application/json' -H "Authorization: Bearer $APIToken" -X GET "https://anypoint.mulesoft.com/exchange/api/v1/organizations/$OrgID/applications" | jq -r ".[] | select(.\"name\" == \"$InternalConsumerApp\") | .\"id\"")
 
init_dynamic_properties

ls -lsa .
#ls -lsa runtime-mgr
 
if [ ! -z "$zip_or_jar_file" ]; then
    deploy_runtime runtime-mgr/"$zip_or_jar_file"
else
    count=`ls -1 runtime-mgr/*.zip 2>/dev/null | wc -l`
    if [ $count != 0 ]; then 
        for i in runtime-mgr/*.zip ; do
            deploy_runtime "$i"
        done
    fi
    count=`ls -1 runtime-mgr/*.jar 2>/dev/null | wc -l`
    if [ $count != 0 ]; then 
        for i in runtime-mgr/*.jar ; do
            deploy_runtime "$i"
        done
    fi
fi
