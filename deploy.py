#!/usr/bin/env python3

import sys
import requests
import json
import os

#python ./deploy.py $EnvironmentID $APIToken $uploadformData $filePath

envId=sys.argv[1]
accessToken=sys.argv[2]
uploadformData=sys.argv[3]
filePath=sys.argv[4]
appExists=sys.argv[5]
apiName=sys.argv[6]

#print(uploadformData + " " + filePath + " " + envId + " " +accessToken+ " "+ appExists+ " "+apiName)
#print(uploadformData)

fileobj = open(filePath, 'rb')

if(appExists=="false"):
    uploadURL = "https://anypoint.mulesoft.com/cloudhub/api/v2/applications"

    multipart_form_data = {
        'file': fileobj,
        'appInfoJson': uploadformData,
        'autoStart': 'true'
    }
        
    uploadResponse = requests.post(uploadURL, headers={"Authorization": "bearer " + accessToken,
                                                       "X-ANYPNT-ENV-ID": envId},
                                   files=multipart_form_data)
    if (uploadResponse.ok):
       print("deployed")
    else:
       print("Deployment Failed" + str(uploadResponse.status_code) + "text = "+str(uploadResponse.text))
       
elif(appExists=="true"):
    redeployApplicationURL = "https://anypoint.mulesoft.com/cloudhub/api/v2/applications/"+apiName

    multipart_form_data = {
        'file': fileobj,
        'appInfoJson': uploadformData
    }

    redeployApplicationResponse = requests.put(redeployApplicationURL,
                                               headers={"Authorization": "bearer " + accessToken,
                                                        "X-ANYPNT-ENV-ID": envId},
                                               files=multipart_form_data)
    if (redeployApplicationResponse.ok):
        print("Deployment Succesfull")
    else:
        print("Deployment Failed " + str(redeployApplicationResponse.status_code) + "text = "+str(redeployApplicationResponse.text))
        
else:
    print("Nothing to deploy")